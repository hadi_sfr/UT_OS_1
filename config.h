#ifndef _CONFIG_H_
#define _CONFIG_H_

#define VERBOSE 1
#define INTERACTIVE 1

#define BUFFER_SIZE 100
#define MAX_DATA_SIZE 1000
#define MAX_BATCH_LEN 10

#endif
