#include <unistd.h>
#include <stdlib.h> // for `malloc` and `free`
/*
    Since malloc.c (http://bazaar.launchpad.net/~vcs-imports/glibc/master/view/head:/malloc/malloc.c)
    has got more than 5000 lines, it seems hard to implement a new version of `malloc` and `free`.
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include "utils.h"
#include "net_utils.h"

const int backlog = 10;

int check_args(int argc, char const *argv[]);
int recv_file_server_data(FileServer *file_servers, char serialized_file_server[]);
int send_file_server_data(int fd, FileServer *const file_servers);

int main(int argc, char const *argv[]) {
    if(check_args(argc, argv) < 0)
        return 1;

    int socket_fd = get_svr_socket(*(argv + 1), *(argv + 2), backlog);
    if(socket_fd == INVALID_SOCKET_FD)
        return 1;
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    FD_SET(socket_fd, &fds);
    int max_fd = socket_fd;
    FileServer *file_servers = malloc(sizeof(FileServer));
    file_servers->next = NULL;
    print_string(STDOUT_FILENO, "Route Server is listening...\n");

    int continue_flag = 1;
    while(continue_flag) {
        fd_set read_fds = fds;
        select(max_fd + 1, &read_fds, NULL, NULL, NULL);
        for(int i = 0; i <= max_fd; i++)
            if(FD_ISSET(i, &read_fds)) {
                if(i == STDOUT_FILENO || i == STDERR_FILENO)
                    continue;
                else if(i == STDIN_FILENO) {
                    char buffer[BUFFER_SIZE + 1];
                    if(!read(STDIN_FILENO, buffer, BUFFER_SIZE)) {
                        continue_flag = 0;
                        break;
                    }
                }
                else if(i == socket_fd) {
                    struct sockaddr_storage client_addr;
                    socklen_t client_addr_size = sizeof(client_addr);
                    int client_fd = accept_extended(
                        socket_fd,
                        (struct sockaddr *)(&client_addr),
                        &client_addr_size);
                    if(client_fd == -1) {
                        print_error("ERR:\tclient_fd", 1);
                        continue;
                    }
                    FD_SET(client_fd, &fds);
                    if(max_fd < client_fd)
                        max_fd = client_fd;
                } else {
                    char buffer[BUFFER_SIZE + 1];
                    if(recv_extended(i, buffer, BUFFER_SIZE, 0) <= 0) {
                        FD_CLR(i, &fds);
                        continue;
                    }
                    NodeType node_type = msg_to_nodetype(buffer);
                    switch(node_type) {
                    case NodeType_RouteServer:
                        print_error("ERR:\tmultiple Route Servers", 1);
                        send_extended(i, ERR501, strlen(ERR501), 0);
                        break;
                    case NodeType_FileServer:
                        recv_file_server_data(file_servers, buffer);
                        break;
                    case NodeType_Client:
                        send_file_server_data(i, file_servers);
                        break;
                    default:
                        print_error("ERR:\tunknown client type", 1);
                        send_extended(i, ERR501, strlen(ERR501), 0);
                        print_error(buffer, 1);
                    }

                    FD_CLR(i, &fds);

                    close_extended(i);
                }
            }
    }

    print_string(STDOUT_FILENO, "Route Server is shutting down...\n");
    close_connections(&fds, max_fd);
    free_FileServer(file_servers);

    return 0;
}

int check_args(int argc, char const *argv[]) {
    if(argc != 3) {
        print_error("usage:\t", 0);
        print_error(argv[0], 0);
        print_error(" <ip> <port>", 1);
        return -1;
    }
    return 0;
}

int recv_file_server_data(FileServer *file_servers, char serialized_file_server[]) {
    FileServer *file_server = malloc(sizeof(FileServer));
    if(string_to_fileserver(file_server, serialized_file_server) < 0) {
        print_error("ERR:\trecv_file_server_data", 1);
        return -1;
    }
    insert_into_file_servers(file_servers, file_server);
    if(VERBOSE) {
        print_error("new File Server added: part ", 0);
        print_number(STDERR_FILENO, file_server->file_part);
        endl(STDERR_FILENO);
    }
    return 0;
}

int send_file_server_data(int fd, FileServer *const file_servers) {
    char s[BUFFER_SIZE + 1];
    fileserver_to_string(file_servers->next, s);
    print_error(s, 1);
    FileServer *head = file_servers;
    while(head->next) {
        head = head->next;
        char s[BUFFER_SIZE + 1];
        fileserver_to_string(head, s);
        char msg[BUFFER_SIZE + 1] = FILE_SERVER;
        merge_strings(msg, " ");
        merge_strings(msg, s);
        if(send_extended(fd, msg, strlen(s), 0) <= 0) {
            print_error("ERR:\tsend_file_server_data", 1);
            return -1;
        }
    }
    if(send_extended(fd, DONE, strlen(DONE), 0) <= 0) {
        print_error("ERR:\tsend_file_server_data", 1);
        return -1;
    }
    return 0;
}
