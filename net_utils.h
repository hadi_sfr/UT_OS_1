#ifndef _NET_UTILS_H_
#define _NET_UTILS_H_

#include <sys/types.h>
#include <sys/socket.h>
#include "config.h"

#define INVALID_SOCKET_FD -1
#define CANCELLED_BY_USER -2

#define IPv4_len 15
#define port_len 6
#define file_part_str_len 3

#define DONE "done"
#define ROUTE_SERVER "route_server"
#define FILE_SERVER "file_server"
#define CLIENT "client"
#define ERR501 "ERR: 501 Not Implemented"
#define MSG_DELIMITER_STR ";"

typedef struct _FileServer
{
    char ip[IPv4_len + 1];
    char port[port_len + 1];
    char file_part_str[file_part_str_len + 1];
    int file_part;
    int fd;
    struct _FileServer *next;
} FileServer;

typedef enum _NodeType {NodeType_RouteServer, NodeType_FileServer, NodeType_Client, NodeType_Unknown} NodeType;

int get_svr_socket(const char *ip, const char *port, int backlog);
int get_clt_socket(const char *ip, const char *port);
int create_clt_socket();
ssize_t recv_extended(int fd, char buffer[], size_t buffer_size, int flag);
ssize_t recv_extended_multi(
    int fd,
    char buffer[MAX_BATCH_LEN][BUFFER_SIZE],
    size_t buffer_array_size,
    size_t buffer_size,
    int flag);
ssize_t send_extended(int fd, const void *buffer, size_t buffer_size, int flag);
int close_connections(fd_set *fds, int max_fd);
int close_extended(int fd);
int accept_extended(int fd, struct sockaddr *addr, socklen_t * len);
void fileserver_to_string(const FileServer *, char []);
int string_to_fileserver(FileServer *, char *);
NodeType msg_to_nodetype(char []);
void free_FileServer(FileServer *head);
void insert_into_file_servers(FileServer *head, FileServer *node);
void get_first_msg(char first_msg[], char msg[], size_t max_len);

#endif
