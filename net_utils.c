#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h> // for `memset`
#include <stdlib.h> // for `malloc`, `free`, and `atoi`
/*
    Since malloc.c (http://bazaar.launchpad.net/~vcs-imports/glibc/master/view/head:/malloc/malloc.c)
    has got more than 5000 lines, it seems hard to implement a new version of `malloc` and `free`.
*/
#include "utils.h"
#include "net_utils.h"

#ifdef __sun
    const char yes = '1';
#else
    const int yes = 1;
#endif

int get_svr_socket(const char *ip, const char *port, int backlog) {
    int return_value = 0;

    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET; //IPv4
    hints.ai_socktype = SOCK_STREAM; //TCP
    if(!simple_strcmp(ip, "localhost"))
        hints.ai_flags = AI_PASSIVE; // use default ip

    struct addrinfo *svr_info, *svr;
    return_value = getaddrinfo(ip, port, &hints, &svr_info);
    if(return_value != 0) {
        print_error("Err:\tgetaddrinfo error:\t", 0);
        print_error(gai_strerror(return_value), 1);
        freeaddrinfo(svr_info);
        return INVALID_SOCKET_FD;
    }

    int socket_fd;
    for(svr = svr_info; svr; svr = svr->ai_next) {
        socket_fd = socket(svr->ai_family, svr->ai_socktype, svr->ai_protocol);
        if(socket_fd == -1){
            print_error("Err:\tsocket", 1);
            continue;
        }
        if(setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
            print_error("Err:\tsetsockopt", 1);
            return INVALID_SOCKET_FD;
        }
        if(bind(socket_fd, svr->ai_addr, svr->ai_addrlen) == -1) {
            close(socket_fd);
            print_error("Err:\tbind", 1);
            return INVALID_SOCKET_FD;
        }
        break;
    }
    freeaddrinfo(svr_info);
    if(!svr) {
        print_error("Err:\tbind failure", 1);
        return INVALID_SOCKET_FD;
    }
    if(listen(socket_fd, backlog) == -1) {
        print_error("Err:\tlisten:\t", 0);
        print_error(strerror(errno), 1);
        return INVALID_SOCKET_FD;
    }

    if(VERBOSE) {
        print_number(STDERR_FILENO, socket_fd);
        print_error(" : svr_socket created", 1);
    }

    return socket_fd;
}

int get_clt_socket(const char *ip, const char *port) {
    int return_value = 0;

    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET; //IPv4
    hints.ai_socktype = SOCK_STREAM; //TCP
    if(!simple_strcmp(ip, "localhost"))
        hints.ai_flags = AI_PASSIVE; // use default ip

    struct addrinfo *svr_info, *svr;
    return_value = getaddrinfo(ip, port, &hints, &svr_info);
    if(return_value != 0) {
        print_error("Err:\tgetaddrinfo error:\t", 0);
        print_error(gai_strerror(return_value), 1);
        freeaddrinfo(svr_info);
        return INVALID_SOCKET_FD;
    }

    int socket_fd;
    for(svr = svr_info; svr; svr = svr->ai_next) {
        socket_fd = socket(svr->ai_family, svr->ai_socktype, svr->ai_protocol);
        if(socket_fd == -1){
            print_error("Err:\tsocket", 1);
            continue;
        }
        if(connect(socket_fd, svr->ai_addr, svr->ai_addrlen) == -1) {
            close(socket_fd);
            print_error("Err:\tconnect", 1);
            return INVALID_SOCKET_FD;
        }
        break;
    }
    freeaddrinfo(svr_info);
    if(!svr) {
        print_error("Err:\tbind failure", 1);
        return INVALID_SOCKET_FD;
    }

    if(VERBOSE) {
        print_number(STDERR_FILENO, socket_fd);
        print_error(" : clt_socket created", 1);
    }

    return socket_fd;
}

int create_clt_socket() {
    int svr_fd;
    do {
        char svr_ip[IPv4_len + 1], svr_port[port_len + 1];
        if(INTERACTIVE) print_string(STDOUT_FILENO, "Server IP:\t");
        if(!read_extended(STDIN_FILENO, svr_ip, IPv4_len)) {
            if(INTERACTIVE) print_string(STDOUT_FILENO, "\n");
            return CANCELLED_BY_USER;
        }
        if(INTERACTIVE) print_string(STDOUT_FILENO, "Server port:\t");
        if(!read_extended(STDIN_FILENO, svr_port, port_len)) {
            if(INTERACTIVE) print_string(STDOUT_FILENO, "\n");
            return CANCELLED_BY_USER;
        }
        svr_fd = get_clt_socket(svr_ip, svr_port);
    } while(svr_fd == INVALID_SOCKET_FD);
    return svr_fd;
}

ssize_t recv_extended(int fd, char msg[], size_t buffer_size, int flag) {
    char buffer[BUFFER_SIZE];
    ssize_t return_value = recv(fd, buffer, buffer_size - 1, flag);
    if(return_value == -1)  {
        print_error("ERR:\trecv:\t", 0);
        print_error(strerror(errno), 1);
    }
    if(return_value == 0)  {
        print_error("ERR:\tconnection closed by client.", 1);
    }
    buffer[return_value] = '\0';
    get_first_msg(msg, buffer, buffer_size);
    trim_end(msg);
    if(VERBOSE) {
        print_number(STDERR_FILENO, fd);
        print_error(" >> ", 0);
        print_error(msg, 1);
    }
    return return_value;
}

ssize_t recv_extended_multi(
        int fd,
        char msg[MAX_BATCH_LEN][BUFFER_SIZE],
        size_t buffer_array_size,
        size_t buffer_size,
        int flag) {
    char buffer[BUFFER_SIZE];
    ssize_t return_value = recv(fd, buffer, buffer_size - 1, flag);
    if(return_value == -1)  {
        print_error("ERR:\trecv:\t", 0);
        print_error(strerror(errno), 1);
    }
    if(return_value == 0)  {
        print_error("ERR:\tconnection closed by client.", 1);
    }
    buffer[return_value] = '\0';
    size_t i;
    for(i = 0; i < MAX_BATCH_LEN && strlen(buffer); i++) {
        get_first_msg(msg[i], buffer, buffer_size);
        trim_end(msg[i]);
        if(VERBOSE) {
            print_number(STDERR_FILENO, fd);
            print_error(" >> ", 0);
            print_error(*(msg + i), 1);
        }
    }
    return i;
}

ssize_t send_extended(int fd, const void *buffer, size_t buffer_size, int flag) {
    char msg[buffer_size + 1];
    simple_strcpy(msg, buffer);
    merge_strings(msg, MSG_DELIMITER_STR);
    ssize_t return_value = send(fd, msg, strlen(msg), flag);
    if(return_value == -1) {
        print_error("ERR:\trecv:\t", 0);
        print_error(strerror(errno), 1);
    }
    if(VERBOSE) {
        print_number(STDERR_FILENO, fd);
        print_error(" << ", 0);
        print_error(buffer, 1);
    }
    return return_value;
}

int close_connections(fd_set *fds, int max_fd) {
    int res = 0;
    for(int i = 0; i <= max_fd; i++) {
        if(i == STDOUT_FILENO || i == STDERR_FILENO || i == STDIN_FILENO)
            continue;
        if(FD_ISSET(i, fds)) {
            FD_CLR(i, fds);
            close_extended(i);
            res++;
        }
    }
    return res;
}

int close_extended(int fd) {
    int return_value = close(fd);
    if(VERBOSE) {
        print_number(STDERR_FILENO, fd);
        print_error(" : terminated by server by return_value ", 0);
        print_number(STDERR_FILENO, return_value);
        endl(STDERR_FILENO);
    }
    return return_value;
}

int accept_extended(int fd, struct sockaddr *addr, socklen_t * len) {
    int new_fd = accept(fd, addr, len);
    if(VERBOSE) {
        print_number(STDERR_FILENO, new_fd);
        print_error(" : com_socket created", 1);
    }
    return new_fd;
}

NodeType msg_to_nodetype(char s[]){
    char token[BUFFER_SIZE + 1];
    get_first_token(s, token, BUFFER_SIZE);
    if(!strcmp(token, ROUTE_SERVER))
        return NodeType_RouteServer;
    if(!strcmp(token, FILE_SERVER))
        return NodeType_FileServer;
    if(!strcmp(token, CLIENT))
        return NodeType_Client;
    return NodeType_Unknown;
}

void fileserver_to_string(const FileServer *file_server, char s[]) {
    if(IPv4_len + port_len + file_part_str_len + 3 > BUFFER_SIZE)
        return;
    s[0] = '\0';
    merge_strings(s, file_server->ip);
    merge_strings(s, " ");
    merge_strings(s, file_server->port);
    merge_strings(s, " ");
    merge_strings(s, file_server->file_part_str);
}

int string_to_fileserver(FileServer *file_server, char *s) {
    get_first_token(s, file_server->ip, IPv4_len);
    get_first_token(s, file_server->port, port_len);
    get_first_token(s, file_server->file_part_str, file_part_str_len);
    file_server->file_part = atoi(file_server->file_part_str);
    file_server->fd = INVALID_SOCKET_FD;
    if(!strlen(file_server->ip) || !strlen(file_server->port) || !strlen(file_server->file_part_str))
        return -1;
    return 0;
}

void insert_into_file_servers(FileServer *head, FileServer *node) {
    while(head->next && head->next->file_part < node->file_part)
        head = head->next;
    node->next = head->next;
    head->next = node;
}

void free_FileServer(FileServer *head) {
    while(head) {
        FileServer *next = head->next;
        free(head);
        head = next;
    }
}

void get_first_msg(char first_msg[], char msg[], size_t max_len) {
    return get_first_part(msg, first_msg, max_len, DelimiterType_SC);
}
