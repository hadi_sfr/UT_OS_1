#include <unistd.h>
#include <stdlib.h> // for `malloc` and `free`
/*
    Since malloc.c (http://bazaar.launchpad.net/~vcs-imports/glibc/master/view/head:/malloc/malloc.c)
    has got more than 5000 lines, it seems hard to implement a new version of `malloc` and `free`.
*/
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/stat.h> // for `fchmod`
#include "utils.h"
#include "net_utils.h"

const int backlog = 10;

int get_file_servers(FileServer *file_servers);
int recv_file_server_data(FileServer *file_servers, char serialized_file_server[]);
int get_file_addr(char addr[], size_t max_len);
int store_data(const char file_addr[], const char data[], const char part[]);

int main(int argc, char const *argv[]) {
    FileServer *file_servers = malloc(sizeof(FileServer));
    file_servers->next = NULL;
    if(get_file_servers(file_servers) < 0)
        return 1;

    char file_addr[BUFFER_SIZE];
    if(get_file_addr(file_addr, BUFFER_SIZE) < 0)
        return 1;

    for(FileServer *p = file_servers->next; p; p = p->next) {
        int file_svr_fd = get_clt_socket(p->ip, p->port);
        if(file_svr_fd == INVALID_SOCKET_FD) {
            print_error("ERR:\tconnecti to FileServer", 1);
            return 1;
        }
        char buffer[BUFFER_SIZE];
        ssize_t res = recv(file_svr_fd, buffer, BUFFER_SIZE, 0);
        if(res <= 0) {
            print_error("ERR:\tget data from FileServer", 1);
            return 1;
        }
        buffer[res] = '\0';
        store_data(file_addr, buffer, p->file_part_str);
        close_extended(file_svr_fd);
    }

    free_FileServer(file_servers);

    return 0;
}

int recv_file_server_data(FileServer *file_servers, char serialized_file_server[]) {
    FileServer *file_server = malloc(sizeof(FileServer));
    if(string_to_fileserver(file_server, serialized_file_server) < 0) {
        print_error("ERR:\trecv_file_server_data", 1);
        return -1;
    }
    insert_into_file_servers(file_servers, file_server);
    if(VERBOSE) {
        print_error("new File Server added: part ", 0);
        print_number(STDERR_FILENO, file_server->file_part);
        endl(STDERR_FILENO);
    }
    return 0;
}

int get_file_servers(FileServer *file_servers) {
    int route_svr_fd = create_clt_socket();
    if(route_svr_fd == CANCELLED_BY_USER)
        return CANCELLED_BY_USER;
    char buffer[MAX_BATCH_LEN][BUFFER_SIZE], token[BUFFER_SIZE];
    int flag = 1;
    if(send_extended(route_svr_fd, CLIENT, strlen(CLIENT), 0) <= 0) {
        print_error("ERR:\tget_file_servers", 1);
        return -1;
    }
    while(flag == 1) {
        size_t batch_len = recv_extended_multi(route_svr_fd, buffer, BUFFER_SIZE, BUFFER_SIZE, 0);
        if(batch_len <= 0) {
            print_error("ERR:\tget_file_servers", 1);
            flag = -1;
            break;
        }
        for(size_t i = 0; i < batch_len; i++) {
            get_first_token(*(buffer + i), token, BUFFER_SIZE);
            if(!strcmp(token, DONE)) {
                flag = 0;
                break;
            }
            else if(!strcmp(token, FILE_SERVER)) {
                if(recv_file_server_data(file_servers, *(buffer + i)) < 0) {
                    flag = -1;
                    break;
                }
            }
            else {
                flag = -1;
                break;
            }
        }
    }
    close_extended(route_svr_fd);
    return flag;
}

int get_file_addr(char addr[], size_t max_len) {
    if(INTERACTIVE) print_string(STDOUT_FILENO, "file address:\t");
    if(!read_extended(STDIN_FILENO, addr, BUFFER_SIZE))
        return CANCELLED_BY_USER;
    return 0;
}

int store_data(const char file_addr[], const char data[], const char part[]) {
    int fd = open(file_addr, O_WRONLY | O_CREAT | O_APPEND);
    if(fd < 0) {
        print_error("ERR:\topen file", 1);
        return -1;
    }
    fchmod(fd, 644);
    size_t written_len = write(fd, data, strlen(data));
    if(written_len <= 0) {
        print_error("ERR:\twrite file", 1);
        close(fd);
        return -1;
    }
    close(fd);
    if(VERBOSE) {
        print_number(STDERR_FILENO, fd);
        print_error(" << [part ", 0);
        print_error(part, 0);
        print_error("]", 1);
    }
    return 0;
}
