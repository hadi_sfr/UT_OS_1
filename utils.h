#ifndef _UTILS_H_
#define _UTILS_H_

#include <sys/types.h>

ssize_t read_extended(int fd, char buffer[], size_t max_len);
ssize_t print_string(int fd, const char*);
ssize_t print_number(int fd, long number);
ssize_t print_error(const char*, int has_endl);
ssize_t endl(int fd);
#ifndef _STRING_H_
size_t strlen(const char *);
int strcmp(const char *, const char *);
#endif
typedef enum _DelimiterType {DelimiterType_WS, DelimiterType_SC} DelimiterType;
void get_first_part(char str[], char token[], size_t max_len, DelimiterType);
void get_first_token(char str[], char token[], size_t max_len);
void trim_end(char *);
void merge_strings(char dst[], const char src[]);
size_t simple_strlen(const char *);
int simple_strcmp(const char *, const char *);
void simple_strcpy(char *dst, const char *src);

#endif
