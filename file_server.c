#include <unistd.h>
#include <fcntl.h>
// #include <stdlib.h> // for `atoi`
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include "utils.h"
#include "net_utils.h"

const int backlog = 10;

int check_args(int argc, char const *argv[]);
int send_details_to_route_server(const FileServer *me);
int get_file_part_number(FileServer *);
int get_data(char data[], size_t max_len);

int main(int argc, char const *argv[]) {
    if(check_args(argc, argv) < 0)
        return 1;

    FileServer me;
    simple_strcpy(me.ip, *(argv + 1));
    simple_strcpy(me.port, *(argv + 2));
    if(get_file_part_number(&me))
        return 1;
    int socket_fd = get_svr_socket(me.ip, me.port, backlog);
    if(socket_fd == INVALID_SOCKET_FD)
        return 1;

    char data[MAX_DATA_SIZE];
    if(get_data(data, MAX_DATA_SIZE) != 0) {
        print_error("ERR:\tread file", 1);
        return 1;
    }

    if(send_details_to_route_server(&me) < 0) {
        print_string(STDOUT_FILENO, "File Server is shutting down...\n");
        close_extended(socket_fd);
        return 1;
    }

    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    FD_SET(socket_fd, &fds);
    int max_fd = socket_fd;
    print_string(STDOUT_FILENO, "File Server is listening...\n");

    int continue_flag = 1;
    while(continue_flag) {
        fd_set read_fds = fds;
        select(max_fd + 1, &read_fds, NULL, NULL, NULL);
        for(int i = 0; i <= max_fd; i++)
            if(FD_ISSET(i, &read_fds)) {
                if(i == STDOUT_FILENO || i == STDERR_FILENO)
                    continue;
                else if(i == STDIN_FILENO) {
                    char buffer[BUFFER_SIZE + 1];
                    if(!read(STDIN_FILENO, buffer, BUFFER_SIZE)) {
                        continue_flag = 0;
                        break;
                    }
                }
                else if(i == socket_fd) {
                    struct sockaddr_storage client_addr;
                    socklen_t client_addr_size = sizeof(client_addr);
                    int client_fd = accept_extended(
                        socket_fd,
                        (struct sockaddr *)(&client_addr),
                        &client_addr_size);
                    if(client_fd == -1) {
                        print_error("ERR:\tclient_fd", 1);
                        continue;
                    }

                    send(client_fd, data, MAX_DATA_SIZE, 0);

                    close_extended(client_fd);
                }
            }
    }

    print_string(STDOUT_FILENO, "File Server is shutting down...\n");
    close_connections(&fds, max_fd);

    return 0;
}

int check_args(int argc, char const *argv[]) {
    if(argc != 3) {
        print_error("usage:\t", 0);
        print_error(argv[0], 0);
        print_error(" <ip> <port>", 1);
        return -1;
    }
    return 0;
}

int send_details_to_route_server(const FileServer *me) {
    int route_svr_fd = create_clt_socket();
    if(route_svr_fd == CANCELLED_BY_USER)
        return CANCELLED_BY_USER;
    char serialized_file_server[BUFFER_SIZE + 1];
    fileserver_to_string(me, serialized_file_server);
    char msg[BUFFER_SIZE + 1] = "";
    merge_strings(msg, FILE_SERVER);
    merge_strings(msg, " ");
    merge_strings(msg, serialized_file_server);
    ssize_t return_value = send_extended(route_svr_fd, msg, strlen(msg), 0);
    if(return_value <= 0)
        print_error("ERR:\tsend_details_to_route_server", 1);
    close_extended(route_svr_fd);
    return return_value <= 0 ? -1 : 0;
}

int get_file_part_number(FileServer *me) {
    if(INTERACTIVE) print_string(STDOUT_FILENO, "file part number:\t");
    if(!read_extended(STDIN_FILENO, (char *)&(me->file_part_str), file_part_str_len)) {
        if(INTERACTIVE) print_string(STDOUT_FILENO, "\n");
        return -1;
    }
    // me->file_part = atoi(me->file_part_str); // atoi is in stdlib.h
    return 0;
}

int get_data(char data[], size_t max_len) {
    char addr[BUFFER_SIZE];
    if(INTERACTIVE) print_string(STDOUT_FILENO, "file address:\t");
    if(!read_extended(STDIN_FILENO, addr, BUFFER_SIZE))
        return CANCELLED_BY_USER;
    int fd = open(addr, O_RDONLY);
    if(fd < 0) {
        print_error("ERR:\topen file", 1);
        return -1;
    }
    size_t res = read(fd, data, max_len);
    if(res <= 0) {
        print_error("ERR:\tread file", 1);
        close(fd);
        return -1;
    }
    data[res] = '\0';
    close(fd);
    if(VERBOSE) {
        print_number(STDERR_FILENO, fd);
        print_error(" >> [file data]", 1);
    }
    return 0;
}
