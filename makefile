CC=gcc
CF=-Wall

all: route_server_output file_server_output client_output

utils: utils.h utils.c
	${CC} ${CF} utils.c -c

net_utils: net_utils.h net_utils.c utils
	${CC} ${CF} net_utils.c -c

route_server: route_server.c utils.h net_utils.h
	${CC} ${CF} route_server.c -c

file_server: file_server.c utils.h net_utils.h
	${CC} ${CF} file_server.c -c

client: client.c utils.h net_utils.h
	${CC} ${CF} client.c -c

route_server_output: route_server utils net_utils
	${CC} ${CF} utils.o net_utils.o route_server.o -o route_server.out

file_server_output: file_server utils net_utils
	${CC} ${CF} utils.o net_utils.o file_server.o -o file_server.out

client_output: client utils net_utils
	${CC} ${CF} utils.o net_utils.o client.o -o client.out

clean:
	rm *.out *.o &>/dev/null