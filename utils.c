#include "utils.h"
#include <unistd.h>

ssize_t read_extended(int fd, char buffer[], size_t max_len) {
    ssize_t return_value = read(fd, buffer, max_len);
    if(return_value < 0)
        return return_value;
    buffer[return_value] = '\0';
    trim_end(buffer);
    return return_value;
}

size_t strlen(const char *c) {
    size_t size = 0;
    while(c && *c) {
        c++;
        size++;
    }
    return size;
}

int strcmp(const char *s1, const char *s2) {
    size_t len = strlen(s1);
    if(strlen(s2) < len)
        return -1;
    if(strlen(s2) > len)
        return 1;
    for(size_t i = 0; i < len; i++) {
        if(s1[i] < s2[i])
            return -1;
        if(s1[i] > s2[i])
            return 1;
    }
    return 0;
}

ssize_t print_string(int fd, const char* c) {
    return write(fd, c, strlen(c));
}

ssize_t print_digit(int fd, int digit) {
    char c[] = "0";
    if(digit >= 0 && digit <= 9) {
        c[0] = digit + '0';
        return print_string(fd, c);
    } else
        return 0;
}

ssize_t print_number(int fd, long number) {
    if(number == 0)
        return print_digit(fd, 0);
    ssize_t res = 0;
    if(number < 1) {
        number *=  -1;
        res += print_string(fd, "-");
    }
    long reverse = 0;
    size_t len = 0;
    while(number) {
        reverse *= 10;
        reverse += number % 10;
        number /= 10;
        len++;
    }
    while(len--) {
        res += print_digit(fd, reverse % 10);
        reverse /= 10;
    }
    return res;
}

ssize_t print_error(const char* msg, int has_endl) {
    size_t res = print_string(STDERR_FILENO, msg);
    if(has_endl)
        res += endl(STDERR_FILENO);
    return res;
}

ssize_t endl(int fd) {
    return print_string(fd, "\n");
}

int is_ws(char c) {
    return (c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\f');
}

void trim_end(char *s) {
    size_t len = strlen(s);
    while(len && is_ws(s[--len]))
        s[len] = '\0';
}

void get_first_part(char str[], char token[], size_t max_len, DelimiterType delimiter) {
    size_t token_size = 0, len = strlen(str);
    if(!len)
        return;
    while(token_size < len && token_size < max_len && str[token_size] && !(
            (delimiter == DelimiterType_WS && is_ws(str[token_size]))
            || (delimiter == DelimiterType_SC && str[token_size] == ';'))) {
        token[token_size] = str[token_size];
        token_size++;
    }
    token[token_size] = '\0';
    if(
        (delimiter == DelimiterType_WS && is_ws(str[token_size]))
        || (delimiter == DelimiterType_SC && str[token_size] == ';'))
        token_size++;
    for(size_t i = 0; i + token_size <= len; i++)
        str[i] = str[i + token_size];
}

void get_first_token(char str[], char token[], size_t max_len) {
    return get_first_part(str, token, max_len, DelimiterType_WS);
}

void merge_strings(char dst[], const char src[]) {
    size_t len = strlen(dst);
    size_t i;
    for(i = 0; src[i]; i++)
        dst[len + i] = src[i];
    dst[len + i] = '\0';
}

size_t simple_strlen(const char *c) {
    return strlen(c);
}

int simple_strcmp(const char *s1, const char *s2) {
    return strcmp(s1, s2);
}

void simple_strcpy(char *dst, const char *src) {
    size_t i;
    for(i = 0; src[i]; i++)
        dst[i] = src[i];
    dst[i] = '\0';
}
